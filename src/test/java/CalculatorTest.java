import org.junit.*;

import static org.junit.Assert.*;


public class CalculatorTest {
    private Calculator start;

    @Before
    public void smg(){
        start = new Calculator();
        System.out.println("Before");
    }

    @After
    public void end(){
        start = new Calculator();
        System.out.println("After");
    }

    @Test
    public void testAddTwoNubmers(){
        assertEquals(30, start.AddTwoNumbers(10,20));
    }

    @Test
    public void testSubtractTwoNumbers(){
        assertEquals(30, start.SubtractTwoNumbers(35,5));
    }

    @Test
    public void testMultiplyTwoNumbers(){
        assertEquals(40 , start.MultiplyTwoNumbers(20,2));
    }

    @Test
    public void testDivideTwoNumbers(){
        assertEquals(5, start.DivideTwoNumbers(50,10));
    }

    @Test
    public void  testDidideTwonubmers2(){
        assertTrue(start.DivideTwoNumbers(20,5)==4);
    }

}
