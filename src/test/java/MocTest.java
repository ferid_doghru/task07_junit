import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.runners.*;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class MocTest {

    @Mock
    ForUser forUser ;

    @BeforeEach
    void smg(){
        System.out.println("Before");
    }

    @AfterEach
    void end(){
        System.out.println("After");
    }

    @Test
   public void testSum(){
lenient().when(forUser.Sum(12,25)).thenReturn(37);
assertEquals(37,forUser.Sum(12,25));
    }

    @Test
    public void testSub(){
        when(forUser.Sub(10,8)).thenReturn(2);
        assertEquals(2,forUser.Sub(10,8));
    }
}
